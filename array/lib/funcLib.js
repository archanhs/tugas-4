//Soal 1
function range(number1, number2) {
    var numbers = [];
    if (number1 <= number2) {
        for (let i = parseInt(number1); i <= number2; i++) {
            numbers.push(i)
        }
    } else {
        for (let i = parseInt(number1); i >= number2; i--) {
            numbers.push(i)
        }
    }
    return numbers;
}
//Soal 2
function rangeStep(number1, number2, step) {
    var numbers = [];
    if (number1 <= number2) {
        for (let i = parseInt(number1); i <= number2; i = i + parseInt(step)) {
            numbers.push(i)
        }
    } else {
        for (let i = parseInt(number1); i >= number2; i = i - parseInt(step)) {
            numbers.push(i)
        }
    }
    return numbers;
}
//Soal 3
function sum(number1, number2 = 1, step = 1) {
    var sum = 0;
    if (number1 <= number2) {
        for (let i = parseInt(number1); i <= number2; i = i + parseInt(step)) {
            sum = sum + i;
        }
    } else {
        for (let i = parseInt(number1); i >= number2; i = i - parseInt(step)) {
            sum = sum + i;
        }
    }
    return "output :" + sum;
}
//Soal 4
function getData(data) {
    console.log("output :")
    for (let i = 0; i < data.length; i++) {
        console.log("Nomor ID:" + data[i][0])
        console.log("Nama Lengkap:" + data[i][1])
        console.log("TTL:" + data[i][2] + " " + data[i][3])
        console.log("Hobi:" + data[i][4])
        console.log(" ")
    }
}
//Soal 5
function balikKata(kata) {
    balik = [];
    for (let i = 0; i < kata.length; i++) {
        balik.push(kata[kata.length-i-1])
    }
    string = balik.join("")
    return "output :" + string;
}


module.exports = {
    range: range,
    rangeStep: rangeStep,
    sum: sum,
    getData: getData,
    balikKata: balikKata,
}