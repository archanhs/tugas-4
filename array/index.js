var funcLib = require('./lib/funcLib');
var data = require('./data.js');
const { getData } = require('./lib/funcLib');

var range = funcLib.range;
var rangeStep = funcLib.rangeStep;
var sum = funcLib.sum;
var balikKata = funcLib.balikKata;

var args = process.argv
var perintah = args[2];
switch (perintah){
    case "range":
        console.log(range(args[3],args[4]))
        break;
    case "rangeWithStep":
        console.log(rangeStep(args[3],args[4],args[5]))
        break;
    case "sum":
        console.log(sum(args[3],args[4],args[5]))
        break;
    case "dataHandling":
        getData(data.input);
        break;
    case "balikKata":
        console.log(balikKata(args[3]));
        break;
    default:
        console.log("perintah tidak terima")
        break;
}
